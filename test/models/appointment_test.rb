require 'test_helper'

class AppointmentTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    @appointment = @user.appointments.build(description: 'Lorem ipsum',
                                            start_time: '2016-10-22 20:39:18',
                                            end_time: '2016-10-22 21:39:18')
  end

  test 'should be valid' do
    assert @appointment.valid?
  end

  test 'user id should be present' do
    @appointment.user_id = nil
    assert_not @appointment.valid?
  end

  test 'description should be present' do
    @appointment.description = '   '
    assert_not @appointment.valid?
  end

  test 'start time should be present' do
    @appointment.start_time = nil
    assert_not @appointment.valid?
  end

  test 'end time should be present' do
    @appointment.end_time = nil
    assert_not @appointment.valid?
  end

  test 'description should be at most 80 characters' do
    @appointment.description = 'a' * 81
    assert_not @appointment.valid?
  end

  test 'end time should follow start time' do
    @appointment.end_time = '2016-10-22 19:39:18'
    assert_not @appointment.valid?
  end

  # test "order should be soonest appointments first" do
  #   assert_equal appointments(:soonest), appointment.first
  # end
end
