# InTime

This is the appointment management application for InTouch Health.

## Application requirements

Users should be able to log in and log out. -DONE

Users should be able to create, update, delete, and see their appointments. -DONE

Users should be able to see each other's appointments (but not update, delete, or create an appointment for another user). -DONE

Users should be able to comment on each other's appointments. -DONE

If an appointment is coming up within the next hour, provide an alert to the user and tell them how many minutes until the appointment. -DONE