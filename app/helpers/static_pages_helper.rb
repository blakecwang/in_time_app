# Static pages helper
module StaticPagesHelper
  def alert_message
    messages = create_messages
    combined = ''
    messages.each do |k|
      combined += k + '_'
    end
    @message = combined
  end

  private

  def create_messages
    appts = fetch_appts
    now = Time.now
    messages = []
    appts.each do |j|
      start = j.start_time.to_time
      if now <= start && now > start - 1.hours
        messages << build_message(j.description, start, now)
      end
    end
    messages
  end

  def fetch_appts
    appts = []
    current_user.appointments.each do |i|
      appts << i
    end
    appts.pop
    appts
  end

  def build_message(desc, start, now)
    'Your appointment, ' + desc + ', is in ' +
      distance_of_time_in_words(start - now) + '.'
  end
end
