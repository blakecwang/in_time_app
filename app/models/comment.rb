# Comment model
class Comment < ApplicationRecord
  belongs_to :appointment
  default_scope -> { order(:created_at) }
  validates :appointment_id, presence: true
  validates :content, presence: true, length: { maximum: 140 }
  validates :author, presence: true
end
