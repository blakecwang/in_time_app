# Application record
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def end_after_start?
    return if [start_time.blank?, end_time.blank?].any?
    errors.add(:end_time, 'must be after start time') if start_time >= end_time
  end
end
