# Appointment model
class Appointment < ApplicationRecord
  belongs_to :user
  has_many :comments, dependent: :destroy
  default_scope -> { order(:start_time) }
  validates :user_id, presence: true
  validates :description, presence: true, length: { maximum: 80 }
  validates :start_time, presence: true
  validates :end_time, presence: true
  validate  :end_after_start?

  def comment_feed
    Comment.where('appointment_id = ?', id)
  end
end
