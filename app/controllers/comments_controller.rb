# Comments controller
class CommentsController < ApplicationController
  before_action :logged_in_user, only: :create

  def create
    @appointment = Appointment.find(params[:comment][:id])
    @comment = @appointment.comments.build(comment_params)
    @comment.author = current_user.id
    if @comment.save
      redirect_to root_url
    else
      @comment_feed_items = []
      render 'static_pages/home'
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:content, :author, :appointment_id)
  end
end
