# Sessions controller
class SessionsController < ApplicationController
  before_action :fetch_user, only: :create

  def new
  end

  def create
    if @user && @user.authenticate(params[:session][:password])
      log_in @user
      redirect_back_or root_url
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end

  private

  def fetch_user
    @user = User.find_by(email: params[:session][:email].downcase)
  end
end
