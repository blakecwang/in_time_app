# Appointments controller
class AppointmentsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy, :edit, :update]
  before_action :correct_user,   only: [:destroy, :edit, :update]

  def create
    @appointment = current_user.appointments.build(appointment_params)
    if @appointment.save
      flash[:success] = 'Appointment created!'
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def destroy
    @appointment.destroy
    flash[:success] = 'Appointment deleted'
    redirect_to root_url
  end

  def edit
  end

  def update
    if @appointment.update_attributes(appointment_params)
      flash[:success] = 'Appointment updated'
      redirect_to root_url
    else
      render 'edit'
    end
  end

  private

  def appointment_params
    params.require(:appointment).permit(:description, :start_time, :end_time)
  end

  def correct_user
    @appointment = current_user.appointments.find_by(id: params[:id])
    redirect_to root_url if @appointment.nil?
  end
end
