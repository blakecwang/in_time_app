user_count = 100
appts_per_user = 3
comments_per_appt = 2

# create users
# User.create!(name: "Navraj Chohan",
#             email: "nchohan@intouchhealth.com",
#             password:              "RajonRails!",
#             password_confirmation: "RajonRails!")

user_count.times do |n|
  name  = Faker::Name.name
  email = "example-#{n + 1}@intouchhealth.com"
  password = 'password'
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end

# create appointments
appt_count = user_count * appts_per_user
user_index = 1
beginning_of_time = DateTime.new(2016, 10, 31, 12, 0, 0, '-7')
appt_count.times do |n|
  description = Faker::Beer.name
  start_time = (beginning_of_time.to_time + n.hours).to_datetime
  end_time = (start_time.to_time + 30.minutes).to_datetime
  User.find(user_index).appointments.create!(description: description,
                                             start_time: start_time,
                                             end_time: end_time)
  # 2.times do
  #   description = Faker::Beer.name
  #   start_time = end_time
  #   end_time = (start_time.to_time + 15.minutes).to_datetime
  #   User.first.appointments.create!(description: description,
  #                                   start_time: start_time,
  #                                   end_time: end_time)
  # end

  if user_index < user_count
    user_index += 1
  else
    user_index = 1
  end
end

# create comments
author = 3
Appointment.all.each do |appointment|
  comments_per_appt.times do
    content = Faker::Hipster.sentence(4)
    appointment.comments.create!(content: content,
                                 author: author)
    if author < user_count
      author += 1
    else
      author = 1
    end
  end
end
