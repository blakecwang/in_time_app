# Create comments
class CreateComments < ActiveRecord::Migration[5.0]
  def change
    create_table :comments do |t|
      t.text :content
      t.integer :author
      t.references :appointment, foreign_key: true

      t.timestamps
    end
    add_index :comments, [:appointment_id, :created_at]
  end
end
